import numpy as np
import pandas as pd
import torch
import torch.nn as nn

# Loads the best model from training
best_model, char_to_int = torch.load("single-char(10epochs0.001lr25seq3layer512hidden).pth", map_location=torch.device('cpu'))
n_vocab = len(char_to_int)
int_to_char = dict((i, c) for c, i in char_to_int.items())


# reload the model
class CharModel(nn.Module):
    def __init__(self):
        super().__init__()
        self.lstm = nn.LSTM(input_size=1, hidden_size=512, num_layers=3, batch_first=True, dropout=0.2)
        self.dropout = nn.Dropout(0.2)
        self.linear = nn.Linear(512, n_vocab)
    def forward(self, x):
        x, _ = self.lstm(x)
        # take only the last output
        x = x[:, -1, :]
        # produce output
        x = self.linear(self.dropout(x))
        return x


model = CharModel()
model.load_state_dict(best_model)
seq_length = 25

# Reads the data and concatenates it to a single document
df = pd.read_csv('./data/backstories.csv')

raw_text = ""
for i in range(0, 5):
    if len(df.loc[i].at['target']) > 10:
        raw_text = raw_text + df.loc[i].at["target"]

# Create test prompt from training data

# Creates a test prompt from
prompt = input("Enter your prompt here: ")
prompt = prompt.lower()
pattern = [char_to_int[c] for c in prompt]
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

model.to(device)

model.eval()
print('Prompt: "%s"' % prompt)
num_char_to_gen = 100
with torch.no_grad():
    for i in range(num_char_to_gen):
        # format input array of int into PyTorch tensor
        x = np.reshape(pattern, (1, len(pattern), 1)) / float(n_vocab)
        x = torch.tensor(x, dtype=torch.float32)


        # generate logits as output from the model
        prediction = model(x.to(device))
        # convert logits into one character
        index = int(prediction.argmax())
        result = int_to_char[index]
        print(result, end="")
        # append the new character into the prompt for the next iteration
        pattern.append(index)
        pattern = pattern[1:]
print()
print("Done.")
