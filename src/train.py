import pandas as pd
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import torch.utils.data as data
from sklearn.model_selection import KFold
import nltk


# Used to Parse the input
def convert_text(train_sample):
    text = train_sample.split('Character')
    return [text[1][7:].strip(), text[2][7:].strip(), text[3][7:].strip().split('\n')[0]]


# Class for the character model
class CharModel(nn.Module):
    def __init__(self):
        super().__init__()
        self.lstm = nn.LSTM(input_size=1, hidden_size=512, num_layers=3, batch_first=True, dropout=0.2)
        self.dropout = nn.Dropout(0.2)
        self.linear = nn.Linear(512, n_vocab)

    def forward(self, x):
        x, _ = self.lstm(x)
        # take only the last output
        x = x[:, -1, :]
        # produce output
        x = self.linear(self.dropout(x))
        return x


# Training Function
def train(model, train_loader, optimizer, device):
    # Train the model
    model.train()

    # Use a for loop to loop through each batch
    for X_batch, y_batch in train_loader:
        # Makes the prediction
        y_pred = model(X_batch.to(device))

        # calculates the loss
        loss = loss_fn(y_pred, y_batch.to(device))

        # zero the gradients
        optimizer.zero_grad()

        # backwards propagation
        loss.backward()
        optimizer.step()


if __name__ == '__main__':
    #df = pd.read_parquet('C:/Users/weinschrottn/dnd_characters_backstories/data/train-00000-of-00001-f131735b1a05d489.parquet')
    df = pd.read_csv('/home/johnsonbr/NLP/data/backstories.csv')

    # Concatenates the input data into a single document
    raw_data = ""
    for i in range(0, len(df["target"])):
        if len(df.loc[i].at['target']) > 10:
            raw_data = raw_data + df.loc[i].at["target"]

    # Set the raw data to lowercase and creates a set of all characters in the raw data
    raw_data = raw_data.lower()
    chars = sorted(list(set(raw_data)))

    # This converts a character into an int
    char_to_int = dict((c, i) for i, c in enumerate(chars))
    pattern = [char_to_int[c] for c in raw_data]

    # summarize the loaded data
    n_chars = len(raw_data)
    n_vocab = len(chars)

    print("Total Characters: ", n_chars)
    print("Total Vocab: ", n_vocab)

    # prepare the dataset of input to output pairs encoded as integers
    seq_length = 25
    dataX = []
    dataY = []
    for i in range(0, n_chars - seq_length, 1):
        seq_in = raw_data[i:i + seq_length]
        seq_out = raw_data[i + seq_length]
        dataX.append([char_to_int[char] for char in seq_in])
        dataY.append(char_to_int[seq_out])

    int_to_char = dict((i, c) for c, i in char_to_int.items())

    print("Number of Items = ", len(char_to_int.items()))

    # Summarize the input to output pairs
    n_patterns = len(dataX)
    print("Total Patterns: ", n_patterns)

    # Sets up the one hot encoding of the data
    X = torch.tensor(dataX, dtype=torch.float32).reshape(n_patterns, seq_length, 1)
    X = X / float(n_vocab)
    y = torch.tensor(dataY)
    print(X.shape, y.shape)

    # Create parameters for the training such as number of epochs, batch size, the k-fold split, and the loss function
    n_epochs = 10
    batch_size = 128
    kf = KFold(n_splits=10, shuffle=True)
    loss_fn = nn.CrossEntropyLoss(reduction="sum")

    # Iterates through each fold
    for fold, (train_idx, test_idx) in enumerate(kf.split(X)):
        print(f"Fold {fold + 1}")
        print("-------")

        # Creates data loaders for both the training and the testing
        train_loader = data.DataLoader(data.TensorDataset(X, y), batch_size=batch_size,
                                       sampler=torch.utils.data.SubsetRandomSampler(train_idx))

        test_loader = data.DataLoader(data.TensorDataset(X, y), batch_size=batch_size,
                                      sampler=torch.utils.data.SubsetRandomSampler(test_idx))

        # Create the model and the optimizer
        model = CharModel()
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        model.to(device)
        optimizer = optim.Adam(model.parameters(), lr=0.001)

        # Train the model for multiple epochs
        for epoch in range(0, n_epochs):
            train(model, train_loader, optimizer, device)

        # Create values to hold the best model and the loss
        best_model = None
        best_loss = np.inf

        # Validation
        model.eval()
        loss = 0
        with torch.no_grad():

            # Create counters that will be used for the performance metrics
            bleu_avg = 0
            counter = 0

            # Goes through each batch in the test Loader and evaluates the models performance
            for X_batch, y_batch in test_loader:
                # Make predictions and calculate the loss of those predictions
                x = np.reshape(pattern, (1, len(pattern), 1)) / float(n_vocab)


                y_pred = model(X_batch.to(device))
                loss += loss_fn(y_pred, y_batch.to(device))

                # empty strings that will hold the true and predicted texts
                p_str = ""
                t_str = ""

                # iterate through every character in a string
                for i in range(0, len(y_batch)):
                    pred = y_pred[i]

                    # get the index of most likely char
                    index = int(pred.argmax())

                    # convert the index integer into a char
                    p_result = int_to_char[index]

                    # Add the character to the prediction string
                    p_str = p_str + p_result

                    true = int(y_batch[i])

                    # convert the index integer into a char
                    t_result = int_to_char[true]

                    # Add the character to the prediction string
                    t_str = t_str + t_result

                #
                hypothesis = p_str.split()
                reference = t_str.split()

                if not hypothesis:
                    bleu_avg += 0
                else:
                    bleu_avg += nltk.translate.bleu_score.sentence_bleu([reference], hypothesis, weights=(0.5, 0.5))

                counter += 1

            # Sets the best model to the one with the lowest loss
            if loss < best_loss:
                best_loss = loss
                best_model = model.state_dict()

            # Print's the loss and MAE of the Model
            print("Cross-entropy: %.4f" % loss)
            print("Bleu Score: " + str(bleu_avg / counter))

        # Saves the best Model
        torch.save([best_model, char_to_int], "single-char(" + str(n_epochs) + "epochs0.001lr" + str(seq_length) + "seq" + "3layer512hidden).pth")
